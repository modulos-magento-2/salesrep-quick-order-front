<?php

namespace Eparts\QuickOrder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Amasty\Perm\Model\DealerOrderFactory;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Event\Observer;
use Magento\User\Model\User;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Class SaveOrderDealer
 * @package Eparts\QuickOrder\Observer
 */
class SaveOrderDealer implements ObserverInterface
{

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var DealerOrderFactory
     */
    protected $dealerOrderFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var User
     */
    protected $user;

    protected $customerRepository;


    /**
     * SaveOrderDealer constructor.
     * @param DealerOrderFactory $dealerOrderFactory
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param User $user
     */
    public function __construct(
        DealerOrderFactory $dealerOrderFactory,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        User $user,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->dealerOrderFactory = $dealerOrderFactory;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->user = $user;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Observer $observer
     * @return $this|false|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $dealerId = $this->customerSession->getDealerId();
        $idCustomerOrder =  $this->customerSession->getIdCustomerQuickOrder();
        if ($dealerId) {
            $name = $this->customerSession->getCustomerData()->getFirstname() . ' ' . $this->customerSession->getCustomerData()->getLastname();

            try {
                $this->dealerOrderFactory->create()
                    ->setDealerId($dealerId)
                    ->setOrderId($order->getId())
                    ->setContactname($name)->save();
            } catch (\Exception $e) {
                return false;
            }

            # Limpa sessão após pedido finalizado
            $adminUser = $this->user->load($this->customerSession->getCustomer()->getEmail(), 'email');
            $groupId = intval($adminUser->getGroupIdDefault());
            $this->customerSession->setCustomerGroupId($groupId);
            $this->customerSession->getCustomer()->setGroupId($groupId)->save();
            $this->customerSession->setDealerId(null);
            $this->customerSession->setIdCustomerQuickOrder(null);
            $this->customerSession->setCustomerQuickOrder(null);

            $customer = $this->customerRepository->getById($idCustomerOrder);
            $order->setCustomerFirstname($customer->getFirstname());
            $order->setCustomerLastname($customer->getLastname());
            $order->setCustomerTaxvat($customer->getTaxvat());
            $order->setCustomerEmail($customer->getEmail());
            $order->getCustomerGroupId($customer->getGroupId());
            $order->save();

        }

        return $this;


    }
}
