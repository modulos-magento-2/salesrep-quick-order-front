<?php

namespace Eparts\QuickOrder\Observer;

use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\User\Model\User;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\ResponseFactory;
use \Magento\Framework\UrlInterface;

/**
 * Class SaveOrderDealer
 * @package Eparts\QuickOrder\Observer
 */
class BlockCheckout implements ObserverInterface
{

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var UrlInterface
     */
    protected $url;


    /**
     * SaveOrderDealer constructor.
     * @param Session $customerSession
     * @param User $user
     * @param ManagerInterface $messageManager
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     */
    public function __construct(
        Session $customerSession,
        User $user,
        ManagerInterface $messageManager,
        ResponseFactory $responseFactory,
        UrlInterface $url
    )
    {
        $this->customerSession = $customerSession;
        $this->user = $user;
        $this->messageManager = $messageManager;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }


    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $emailCustomer = $this->customerSession->getCustomer()->getEmail();
        if (empty($emailCustomer)) {
            return $this;
        }

        $adminUser = $this->user->load($emailCustomer, 'email');

        if (!empty($adminUser) && $adminUser->getEmail() == $emailCustomer) {
            $customerQuickOrder = $this->customerSession->getIdCustomerQuickOrder();
            if (empty($customerQuickOrder)) {
                $this->messageManager->addErrorMessage('Você precisa selecionar um cliente para finalizar a compra por ele, se não deseja finalizar a compra por ele, compartilhe o carrinho com o seu cliente.');
                $redirectionUrl = $this->url->getUrl('checkout/cart/index');
                $this->responseFactory->create()->setRedirect($redirectionUrl)->sendResponse();

                return $this;
            }
        }

        return $this;
    }
}
