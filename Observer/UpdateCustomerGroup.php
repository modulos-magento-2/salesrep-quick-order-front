<?php

namespace Eparts\QuickOrder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Amasty\Perm\Model\DealerOrderFactory;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\User\Model\User;

/**
 * Class SaveOrderDealer
 * @package Eparts\QuickOrder\Observer
 */
class UpdateCustomerGroup implements ObserverInterface
{

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var DealerOrderFactory
     */
    protected $dealerOrderFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var User
     */
    protected $user;


    /**
     * SaveOrderDealer constructor.
     * @param DealerOrderFactory $dealerOrderFactory
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param User $user
     */
    public function __construct(
        DealerOrderFactory $dealerOrderFactory,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        User $user
    )
    {
        $this->dealerOrderFactory = $dealerOrderFactory;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->user = $user;
    }

    /**
     * @param Observer $observer
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException|\Exception
     */
    public function execute(Observer $observer)
    {
        $customerQuickOrder = $this->customerSession->getIdCustomerQuickOrder();
        if (!$customerQuickOrder) {
            $adminUser = $this->user->load($this->customerSession->getCustomer()->getEmail(), 'email');
            if ($adminUser->getData()) {
                $groupIdDefault = intval($adminUser->getGroupIdDefault());
                $this->customerSession->setCustomerGroupId($groupIdDefault);
                $this->customerSession->getCustomer()->setGroupId($groupIdDefault)->save();
            }
        }
        return $this;


    }
}
