<?php

namespace Eparts\QuickOrder\Controller\Cart;

use Magento\Framework\Controller\ResultFactory; 

class Newcart extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $request;
    protected $session;
    protected $urlBuilder;
    protected $customerFactory;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productFactory;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected  $quoteFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;


    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    protected $messageManager;


    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Customer\Model\Session $session ,
     * @param \Magento\Framework\UrlInterface $urlBuilder ,
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession ,
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->session = $session;
        $this->urlBuilder = $urlBuilder;
        $this->customerFactory = $customerFactory;
        $this->productFactory = $productFactory;
        $this->checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;
        $this->customerSession = $customerSession;
        $this->quoteRepository = $quoteRepository;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
    }

    public function execute()
    {
        try {
            if ( $this->session->isLoggedIn()) {
                $quote = $this->quoteFactory->create()->load($this->getQuoteId());
                $this->checkoutSession->setQuoteId(null);
                $this->checkoutSession->clearHelperData();
                $quote->setIsActive(false);
                $this->quoteRepository->save($quote);

                $store = $this->storeManager->getStore();
                $customer = $this->customerRepository->getById($this->session->getCustomerId());

                //$websiteId = $this->storeManager->getStore()->getWebsiteId();
                $newQuote = $this->quoteFactory->create();
                $newQuote->setStore($store); // Set Store
                $newQuote->setCurrency();
                $newQuote->assignCustomer($customer);
                $newQuote->save();
                $id  = $newQuote->getId();
                $this->messageManager->addSuccess(__("Foi criado um novo carrinho #$id. Adicione novos itens!"));
               
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__("Não foi possível criar um novo carrinho, tente novamente!"));
        }

        return $this->redirectFactory->create()->setUrl('/quick-order');

    }
    /**
     * Checkout quote id
     *
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuoteId()
    {
        return (int)$this->checkoutSession->getQuote()->getId();
    }
}


