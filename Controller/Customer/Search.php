<?php

namespace Eparts\QuickOrder\Controller\Customer;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Eparts\QuickOrder\Helper\Customer as HelperCustomer;

/**
 * Class Save
 * @package Eparts\QuickOrder\Controller\Customer
 */
class Search implements HttpGetActionInterface
{

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var HelperCustomer
     */
    protected $helperCustomer;


    /**
     * Delete constructor.
     * @param RequestInterface $request
     * @param JsonFactory $jsonFactory
     * @param HelperCustomer $helperCustomer
     */
    public function __construct(
        RequestInterface $request,
        JsonFactory $jsonFactory,
        HelperCustomer $helperCustomer
    )
    {
        $this->request = $request;
        $this->jsonFactory = $jsonFactory;
        $this->helperCustomer = $helperCustomer;
    }

    /**
     * @return Json
     * @throws \Exception
     */
    public function execute(): Json
    {
        $value = $this->request->getParam('value');
        $ids = $this->request->getParam('ids');

        $customers = $this->helperCustomer->searchCustomer($ids, $value);

        return $this->jsonFactory->create()->setData($customers);
    }
}
