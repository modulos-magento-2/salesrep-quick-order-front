<?php

namespace Eparts\QuickOrder\Controller\Customer;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\User\Model\User;

/**
 * Class Save
 * @package Eparts\QuickOrder\Controller\Customer
 */
class Save implements HttpGetActionInterface
{

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    /**
     * @var ManagerInterface
     */
    protected $manager;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var User
     */
    protected $userFactory;

    /**
     * Delete constructor.
     * @param ManagerInterface $manager
     * @param RequestInterface $request
     * @param RedirectFactory $redirectFactory
     * @param Session $session
     * @param JsonFactory $jsonFactory
     * @param User $userFactory
     */
    public function __construct(
        ManagerInterface $manager,
        RequestInterface $request,
        RedirectFactory $redirectFactory,
        Session $session,
        JsonFactory $jsonFactory,
        User $userFactory
    )
    {
        $this->manager = $manager;
        $this->request = $request;
        $this->redirectFactory = $redirectFactory;
        $this->session = $session;
        $this->jsonFactory = $jsonFactory;
        $this->userFactory = $userFactory;
    }


    /**
     * @return Redirect
     * @throws \Exception
     */
    public function execute(): Redirect
    {
        $customerId = $this->request->getParam('idCustomer');
        $groupId = $this->request->getParam('group');
        $name = $this->request->getParam('name');

        if (empty($customerId)) {
            $this->session->setIdCustomerQuickOrder(null);
            $this->session->setCustomerQuickOrder(null);
            return $this->redirectFactory->create()->setUrl('/quick-order');
        }

        $this->session->setIdCustomerQuickOrder($customerId);
        $this->session->setCustomerQuickOrder($name);
        $this->session->setCustomerGroupId($groupId);
        $this->session->getCustomer()->setGroupId($groupId)->save();


        return $this->redirectFactory->create()->setUrl('/quick-order');
    }
}
