<?php

namespace Eparts\QuickOrder\Plugin;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\RequestInterface;

/**
 * Class SharedCart
 * @package Eparts\QuickOrder\Plugin
 */
class SharedCart
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * SharedCart constructor.
     * @param Cart $cart
     * @param RequestInterface $request
     */
    public function __construct(
        Cart $cart,
        RequestInterface $request
    )
    {
        $this->cart = $cart;
        $this->request = $request;
    }

    /**
     * @throws Exception
     */
    public function beforeExecute()
    {
        $params = $this->request->getParams();
        $groupId = $params['group_id'];

        if ($groupId) {
            $this->cart->getQuote()
                ->setCustomerGroupId($groupId)
                ->save();
        }

    }
}
