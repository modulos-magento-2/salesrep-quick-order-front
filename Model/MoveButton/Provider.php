<?php

declare(strict_types=1);

namespace Eparts\QuickOrder\Model\MoveButton;

use Amasty\QuickOrder\Model\GetIsRequestQuoteEnabled;
use Magento\Framework\UrlInterface;

class Provider extends \Amasty\QuickOrder\Model\MoveButton\Provider
{
     /**
     * @var UrlInterface
     */
    protected $urlBuilder2;

    /**
     * @var GetIsRequestQuoteEnabled
     */
    protected $getIsRequestQuoteEnabled;

    public function __construct(
        GetIsRequestQuoteEnabled $getIsRequestQuoteEnabled,
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder2 = $urlBuilder;
        parent::__construct($getIsRequestQuoteEnabled, $urlBuilder);
    }
   
    public function getButtons(string $mode, array $requestedButtons): array
    {
        $buttons = [];

        if (in_array(self::QUOTE_BUTTON, $requestedButtons)) {
            $buttons[] = $this->getRequestQuoteButton($mode);
        }
        if (in_array(self::CART_BUTTON, $requestedButtons)) {
            $buttons[] = $this->getCartButton($mode);
        }
        // if (in_array(self::CHECKOUT_BUTTON, $requestedButtons)) {
        //     $buttons[] = $this->getCheckoutButton($mode);
        // }

        return array_values(array_filter($buttons));
    }


    protected function getCartButton(string $mode): array
    {
        return [
            'title' => __('Fechar Carrinho'),
            'classes' => 'amqorder-button -empty -cart',
            'url' => $this->urlBuilder2->getUrl(static::URL_MAP[$mode]['cart'])
        ];
    }

   
}
