# Changelog
Todas as mudanças neste projeto serão documentadas neste arquivo.

## [Unreleased]

## [1.1.4] - 2022-01-31
### Fixed
- Corrigido erro no carrinho para usuário não logados

