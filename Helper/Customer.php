<?php

namespace Eparts\QuickOrder\Helper;

use Magento\Customer\Model\ResourceModel\Customer\Collection as CustomerCollection;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class Customer
 * @package Eparts\QuickOrder\Helper
 */
class Customer
{

    protected $customerCollection;

    public function __construct(
        CustomerCollection $customerCollection
    )
    {
        $this->customerCollection = $customerCollection;
    }

    /**
     * @param $ids
     * @param $value
     * @return array
     */
    public function searchCustomer($ids, $value): array
    {
        $collection = $this->getCustomers($ids, $value);
        $data = [];
        foreach ($collection as $customer) {
            $data[] = [
                'name' => $customer->getFirstname() . ' ' . $customer->getLastname() . ' - ' . $customer->getTaxvat(),
                'id' => $customer->getEntityId(),
                'group_id' => $customer->getGroupId()
            ];
        }

        return $data;
    }

    /**
     * @param $ids
     * @param $value
     * @return CustomerCollection|AbstractDb
     */
    protected function getCustomers($ids, $value)
    {
        $field = 'firstname';
        if (is_numeric($value)) {
            $field = 'taxvat';
        }
        return $this->customerCollection
            ->addFieldToFilter('entity_id', ['in' =>  $ids])
            ->addFieldToFilter($field, ['like' => '%' . $value . '%'])
            ->setOrder('name', 'ASC');
    }
}
