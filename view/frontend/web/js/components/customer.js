
define([
    'jquery',
    'ko',
    'uiComponent',
    'mage/url'
], function ($, ko, Component, urlBuilder) {
    'use strict';

    return Component.extend({
        /**
         * Initializes component
         */
        initialize: function () {
            var self = this;
            self._super();
        },

    });
});
