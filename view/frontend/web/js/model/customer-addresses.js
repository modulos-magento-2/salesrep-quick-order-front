/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'ko',
    './customer/address'
], function ($, ko, Address) {
    'use strict';

    var isLoggedIn = ko.observable(window.isCustomerLoggedIn);

    return {
        /**
         * @return {Array}
         */
        getAddressItems: function () {
            var items = [],
                customerData = window.customerData;

            var AddressQuickOrderCustomer = window.AddressCustomerQuickOrder;

            if (isLoggedIn()) {
                if (Object.keys(AddressQuickOrderCustomer).length) {
                    $.each(AddressQuickOrderCustomer, function (key, item) {
                        items.push(new Address(item));
                    });
                } else {
                    if (Object.keys(customerData).length) {
                        $.each(customerData.addresses, function (key, item) {
                            items.push(new Address(item));
                        });
                    }
                }
            }

            return items;
        }
    };
});
