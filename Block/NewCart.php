<?php

namespace Eparts\QuickOrder\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\User\Model\UserFactory;

/**
 * Class NewCart
 * @package Eparts\QuickOrder\Block
 */
class NewCart extends Template
{
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Cart
     */
    protected $cart;
    /**
     * @var Session
     */
    protected $session;

    /**
     * NewCart constructor.
     * @param Context $context
     * @param Registry $registry
     * @param priceHelper $priceHelper
     * @param Cart $cart
     * @param Session $session
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Cart $cart,
        Session $session,
        UserFactory $adminUser,
        array $data = []
    ) {
    
        $this->registry = $registry;
        $this->cart = $cart;
        $this->session = $session;
        $this->adminUser = $adminUser;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getCurrentStoreId(){
        try {
            return $this->_storeManager->getStore()->getId();
        }catch (\Exception $e){
            $this->helper->printLog($e->getMessage());
        }
    }

    /**
     * @return string $url
     */
    public function getNewCartUrl(){
        $url = $this->getUrl('eparts_quickorder/cart/newcart');

        return $url;
    }

    public function showButton(){
        $customerData = $this->session->getCustomerData();
        if($customerData){
            $email = $customerData->getEmail();
            $adminUser = $this->adminUser->create()->load($email, 'email');

            if ($adminUser->getData()) {
                $role  = $adminUser->getRole()->getData();
                $role_name = $role["role_name"];
                if($role_name == "Vendedor"){
                    return true;
                }
            }
        }
        return false;
    }
}
