<?php

namespace Eparts\QuickOrder\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Amasty\Perm\Model\DealerCustomer;
use Amasty\Perm\Model\Dealer;
use Magento\User\Model\UserFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\ResourceModel\Customer\Collection as CustomerCollection;


/**
 * Class Customer
 * @package Amasty\QuickOrder\Block
 */
class Customer extends Template
{

    /**
     * @var DealerCustomer
     */
    protected $dealerCustomer;

    /**
     * @var Dealer
     */
    protected $dealer;

    /**
     * @var UserFactory
     */
    protected $adminUser;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    protected $customerCollection;

    /**
     * Customer constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param DealerCustomer $dealerCustomer
     * @param Dealer $dealer
     * @param UserFactory $adminUser
     * @param Session $customerSession
     * @param CustomerCollection $customerCollection
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        DealerCustomer $dealerCustomer,
        Dealer $dealer,
        UserFactory $adminUser,
        Session $customerSession,
        CustomerCollection $customerCollection,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->customerRepository = $customerRepository;
        $this->dealerCustomer = $dealerCustomer;
        $this->dealer = $dealer;
        $this->adminUser = $adminUser;
        $this->customerSession = $customerSession;
        $this->customerCollection = $customerCollection;
    }

    /**
     * @return false|string
     */
    public function getJsLayout()
    {
        return json_encode($this->jsLayout, JSON_HEX_TAG);
    }

    /**
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomersByDealer()
    {
        $customer = $this->customerSession->getCustomerData();
        $email = $customer->getEmail();

        $adminUser = $this->adminUser->create()->load($email, 'email');
        if ($adminUser->getData()) {
            $idDealer = $adminUser->getId();
            $dealer = $this->dealer->load($idDealer, 'user_id');
            $customers = $this->dealerCustomer->getCustomers($dealer);
            if (!empty($customers)) {
                $this->customerSession->setDealerId($dealer->getEntityId());
                return $customers;
            }
        }

        return [];

    }

    /**
     * @return mixed
     */
    public function getCustomerSelect()
    {
        return $this->customerSession->getCustomerQuickOrder();
    }
}
