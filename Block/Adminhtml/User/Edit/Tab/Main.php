<?php
namespace Eparts\QuickOrder\Block\Adminhtml\User\Edit\Tab;

use Magento\Backend\Block\Widget\Form;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Locale\OptionInterface;

/**
 * Class Main
 * @package Eparts\QuickOrder\Block\Adminhtml\User\Edit\Tab
 */
class Main extends \Magento\User\Block\User\Edit\Tab\Main
{

    /**
     * @var Collection
     */
    protected $customerGroup;

    public function __construct(
        Collection $collection,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        array $data = [],
        OptionInterface $deployedLocales = null
    ) {
        $this->customerGroup = $collection;
        parent::__construct($context,$registry,$formFactory,$authSession,$localeLists,$data,$deployedLocales);
    }
    /**
     * Prepare form fields
     *
     * @return Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $model = $this->_coreRegistry->registry('permissions_user');
        $baseFieldset = $form->getElement('base_fieldset');
        $baseFieldset->addField(
            'group_id_default',
            'select',
            [
                'name' => 'group_id_default',
                'label' => __('Tabela de Preço Padrão'),
                'title' => __('Tabela de Preço Padrão'),
                'value' => $model->getGroupIdDefault(),
                'values' => $this->customerGroup->toOptionArray()
            ]
        );
        return $this;
    }


}
