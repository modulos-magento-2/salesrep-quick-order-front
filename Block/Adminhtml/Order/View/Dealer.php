<?php

namespace Eparts\QuickOrder\Block\Adminhtml\Order\View;

use Amasty\Perm\Model\DealerOrder;
use Magento\Framework\App\RequestInterface;
use Amasty\Perm\Model\Dealer as ModelDealer;
use Magento\User\Model\User;
use Magento\Backend\Block\Template;

/**
 * Class Dealer
 * @package Eparts\QuickOrder\Block\Adminhtml\Order\View
 */
class Dealer extends \Magento\Backend\Block\Template
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var DealerOrder
     */
    protected $orderDealer;

    /**
     * @var ModelDealer
     */
    protected $dealer;

    /**
     * @var User
     */
    protected $userAdmin;

    /**
     * Dealer constructor.
     * @param RequestInterface $request
     * @param DealerOrder $orderDealer
     * @param ModelDealer $dealer
     * @param User $userAdmin
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        RequestInterface $request,
        DealerOrder $orderDealer,
        ModelDealer $dealer,
        User $userAdmin,
        Template\Context $context,
        array $data = []
    )
    {
        $this->request = $request;
        $this->orderDealer = $orderDealer;
        $this->dealer = $dealer;
        $this->userAdmin = $userAdmin;

        parent::__construct($context, $data);
    }

    public function getDataDealer()
    {
        $orderId = $this->request->getParam('order_id');
        $orderDealer = $this->orderDealer->load($orderId, 'order_id');
        if ($orderDealer->getData()) {
            $idDealer = $orderDealer->getDealerId();
            $dealer = $this->dealer->load($idDealer);
            if ($dealer->getData()) {
                $userId = $dealer->getUserId();
                $user = $this->userAdmin->load($userId);
                if ($user->getData()) {
                    return [
                        'name' => $user->getData('username'),
                        'document' => $user->getData('document_dealer')
                    ];
                }
            }
        }

        return false;
    }
}