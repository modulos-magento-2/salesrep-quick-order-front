<?php

namespace Eparts\QuickOrder\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Meetanshi\WhatsappShareCart\Block\WhatsappShareCart;
use Magento\Framework\View\Element\Template\Context;
use Meetanshi\WhatsappShareCart\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\Pricing\Helper\Data as priceHelper;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer\Source\Group;


/**
 * Class SharedCart
 * @package Eparts\QuickOrder\Block
 */
class SharedCart extends WhatsappShareCart
{
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var priceHelper
     */
    protected $priceHelper;
    /**
     * @var Cart
     */
    protected $cart;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var Group
     */
    protected $groupData;

    /**
     * WhatsappShareCart constructor.
     * @param Context $context
     * @param Registry $registry
     * @param priceHelper $priceHelper
     * @param Data $helper
     * @param Cart $cart
     * @param Session $session
     * @param Group $groupData
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        priceHelper $priceHelper,
        Data $helper,
        Cart $cart,
        Session $session,
        Group $groupData,
        array $data = []
    ) {

        $this->helper = $helper;
        $this->priceHelper = $priceHelper;
        $this->registry = $registry;
        $this->cart = $cart;
        $this->session = $session;
        $this->groupData = $groupData;

        parent::__construct($context,$registry,$priceHelper,$helper,$cart,$session,$groupData);
    }

    /**
     * @return array|string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCartData()
    {
        $customerGroup = 0;
        if ($this->session->getIdCustomerQuickOrder()) {
            $customerGroup = $this->session->getCustomerGroupId();
        }

        try {
            $cartConfig = $this->helper->getConfigValue();
            $id = $this->cart->getQuote()->getId();
            $items = $this->cart->getQuote()->getAllVisibleItems();
            $data = "";
            if ($cartConfig['custom_message'] != '') :
                $data .= $cartConfig['custom_message'] . "\r\n\r\n";
            endif;
            foreach ($items as $item) {
                $data .= $item->getName() . " -- ";
                $data .= $this->priceHelper->currency($item->getPrice(), true, false) . "\r\n\r\n";
            }
            $data .= "Para visualizar os produtos, clique no link" . "\r\n";
            $url = $this->getUrl('sharecart/index/index', ['id' => $id, 'group_id' => $customerGroup]);
            if ($cartConfig['utm_enable']) :
                $url = $this->getUtmTrackUrl($url);
            endif;
            if ($cartConfig['bitly_enable']) :
                $url = $this->getBitlyUrl($url);
            endif;
            $data .= $url;

            return str_replace('+', ' ', urlencode($data));
        } catch (\Exception $e) {
            $this->helper->printLog($e->getMessage());
            return "";
        }
    }
}
