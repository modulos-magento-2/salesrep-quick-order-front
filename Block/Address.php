<?php

namespace Eparts\QuickOrder\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Customer;

/**
 * Class Address
 * @package Eparts\QuickOrder\Block
 */
class Address extends Template
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * MarriedCart constructor.
     * @param Template\Context $context
     * @param CustomerSession $customerSession
     * @param AddressRepositoryInterface $addressRepository
     * @param Customer $customer
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CustomerSession $customerSession,
        AddressRepositoryInterface $addressRepository,
        Customer $customer,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;
        $this->addressRepository = $addressRepository;
        $this->customer = $customer;
        parent::__construct($context, $data);
    }


    /**
     * @return false|string
     * @throws LocalizedException
     */
    public function getAddresses()
    {
        $customerId = $this->customerSession->getIdCustomerQuickOrder();
        if (empty($customerId)) {
            return json_encode([]);
        }

        $customerData = $this->customer->load($customerId);
        $addresses = $customerData->getAddresses();
        $data = [];
        foreach ($addresses as $address) {
            $addressData = $this->addressRepository->getById($address->getId());
            $data[$address->getId()] = [
                'city' => $addressData->getCity(),
                'company' => $addressData->getCompany(),
                'country_id' => $addressData->getCountryId(),
                'customer_id' => $addressData->getCustomerId(),
                'default_billing' => $addressData->isDefaultBilling(),
                'default_shipping' => $addressData->isDefaultShipping(),
                'fax' => $addressData->getFax(),
                'firstname' => $addressData->getFirstname(),
                'id' => $addressData->getId(),
                'inline' => $addressData->getFirstname() . ' - ' . $addressData->getStreet()[0],
                'lastname' => $addressData->getLastname(),
                'middlename' => '',
                'postcode' => $addressData->getPostcode(),
                'prefix' => $addressData->getPrefix(),
                'region' => [
                    'region' => $addressData->getRegion()->getRegion(),
                    'region_code' => $addressData->getRegion()->getRegionCode(),
                    'region_id' => $addressData->getRegion()->getRegionId(),
                ],
                'region_id' => $addressData->getRegion()->getRegionId(),
                'street' => $addressData->getStreet(),
                'suffix' => $addressData->getSuffix(),
                'telephone' => $addressData->getTelephone(),
                'vat_id' => $addressData->getVatId(),
            ];
        }
        return json_encode($data);
    }
}
